# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.pairingplugin` plug-in for the [Dynamix Framework](http://ambientdynamix.org). It allows users to initiate new remote pairings with the Dynamix Framework.

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Dependencies
* The plugin depends on two other Dynamix plugins - Barcode Context Plugin and NFC To Interact.

## Context Support
* The plugin does not offer any context supports but uses a dummy context type as required by the framework.

## Features Supported
The plugin supports three pairing mechanisms which can be initiated using the Features fragment of the Dynamix Framework.

1. `Scan To Interact` : Scan a QR Code using the camera to read pairing data. Used by Dynamix JS.
2. `Generate Pairing Code` : This feature displays a pre-authorized pairing code which can then be used by apps to initiate remote pairing.
3. `NFC To Interact` : Scan an NFC Tag to receive pairing data. This method is currently used by the DynamixPy library running on a Raspberry Pi.


