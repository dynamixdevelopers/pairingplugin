/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.pairingplugin;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.api.contextplugin.*;
import org.ambientdynamix.contextplugins.barcode.BarcodeContextInfo;
import org.ambientdynamix.contextplugins.nfctointeract.NFCInfo;
import org.json.JSONObject;

import java.util.Random;
import java.util.UUID;

public class PairingPluginRuntime extends ContextPluginRuntime {
    private final String TAG = this.getClass().getSimpleName();
    private Context context;
    private DynamixFacade dynamix;
    private ContextHandler contextHandler;
    private String NFC_TO_INTERACT_PLUGIN_ID = "org.ambientdynamix.contextplugins.nfctointeract";
    private String SCAN_TO_INTERACT_PLUGIN_ID = "org.ambientdynamix.contextplugins.barcode";
    Bundle alert = new Bundle();

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(powerScheme);
        this.context = this.getSecuredContext();
    }

    @Override
    public void start() {
        if (contextHandler == null) {
            try {
                dynamix = getPluginFacade().getDynamixFacadeWrapper(getSessionId());
                dynamix.openSession(new SessionCallback() {
                    @Override
                    public void onSuccess(DynamixFacade dynamixFacade) throws RemoteException {
                        dynamix.createContextHandler(new ContextHandlerCallback() {

                            @Override
                            public void onSuccess(ContextHandler handler) throws RemoteException {
                                contextHandler = handler;
                                contextHandler.addContextSupport(SCAN_TO_INTERACT_PLUGIN_ID, BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE, new ContextSupportCallback() {
                                    @Override
                                    public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                                        Log.i(TAG, "support succesfully added");
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) throws RemoteException {
                                        Log.w(TAG,
                                                "Call was unsuccessful! Message: " + message + " | Error code: "
                                                        + errorCode);
                                    }
                                });
                                /*
                                contextHandler.addContextSupport(NFC_TO_INTERACT_PLUGIN_ID, NFCInfo.CONTEXT_TYPE, new ContextSupportCallback() {
                                    @Override
                                    public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
                                        Log.i(TAG, "support succesfully added");
                                    }

                                    @Override
                                    public void onFailure(String message, int errorCode) throws RemoteException {
                                        Log.w(TAG,
                                                "Call was unsuccessful! Message: " + message + " | Error code: "
                                                        + errorCode);
                                    }
                                });
                                */
                            }

                            @Override
                            public void onFailure(String message, int errorCode) throws RemoteException {
                                Log.e(TAG, message);
                            }
                        });
                    }

                    @Override
                    public void onFailure(String s, int i) throws RemoteException {

                    }
                });

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {

    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {

    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {

    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {

    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return true;
    }

    public void runScanToInteract() {
        ContextRequestCallback scanToInteractCallback = new ContextRequestCallback() {
            @Override
            public void onSuccess(ContextResult result) throws RemoteException {
                try {
                    BarcodeContextInfo info = (BarcodeContextInfo) result.getIContextInfo();
                    String data = info.getBarcodeValue();
                    JSONObject object = new JSONObject(data);
                    String barcodeAction = "";
                    if (object.has("TYPE")) {
                        barcodeAction = object.getString("TYPE");
                    }
                    if (barcodeAction.trim().equalsIgnoreCase("PAIRING")) {
                        String lookup_key = object.getString("LOOKUP_KEY");
                        String name = object.getString("APP_NAME");
                        String type = object.getString("APP_TYPE");
                        String pairingCode = object.getString("PAIRING_CODE");
                        Bundle bundle = new Bundle();
                        bundle.putString("OPERATION", "ADD_DYNAMIX_APPLICATION_OPERATION");
                        bundle.putString("PAIRING_CODE", pairingCode);
                        bundle.putString("APP_NAME", name);
                        bundle.putString("LOOKUP_KEY", lookup_key);
                        bundle.putString("APP_TYPE", type);
                        sendPairingInfoToDynamix(bundle);
                    } else {
                        Log.e(getClass().getSimpleName(), "Unsupported Barcode action: " + barcodeAction);
                        alert = new Bundle();
                        alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
                        alert.putString(PluginConstants.ALERT_TOAST_VALUE, "Could not retrieve pairing data from QR Code " + barcodeAction);
                        alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
                        sendPluginAlert(alert);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(getClass().getSimpleName(), e.getMessage());
                    alert = new Bundle();
                    alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
                    alert.putString(PluginConstants.ALERT_TOAST_VALUE, "Something bad happened. Please scan the QR Code again.");
                    alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
                    sendPluginAlert(alert);
                }
            }


            @Override
            public void onFailure(String message, int errorCode) throws RemoteException {
                alert = new Bundle();
                alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
                alert.putString(PluginConstants.ALERT_TOAST_VALUE, message);
                alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
                sendPluginAlert(alert);
            }
        };

        try {
            if (contextHandler != null)
                contextHandler.contextRequest(SCAN_TO_INTERACT_PLUGIN_ID, BarcodeContextInfo.BARCODE_SCAN_CONTEXT_TYPE, scanToInteractCallback);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    /*
    public void runNFCToInteract() {
        ContextRequestCallback nfcToInteractCallback = new ContextRequestCallback() {
            @Override
            public void onSuccess(ContextResult result) throws RemoteException {
                NFCInfo info = (NFCInfo) result.getIContextInfo();
                String fullText = info.getPayload();
                String pairingCode = fullText.substring(fullText.length() - 12);
                sendPairingInfoToDynamix(pairingCode);
            }

            @Override
            public void onFailure(String message, int errorCode) throws RemoteException {
                alert = new Bundle();
                alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
                alert.putString(PluginConstants.ALERT_TOAST_VALUE, message);
                alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
                sendPluginAlert(alert);
            }
        };

        try {
            contextHandler.contextRequest(NFC_TO_INTERACT_PLUGIN_ID, NFCInfo.CONTEXT_TYPE, nfcToInteractCallback);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
    */

    private void sendPairingInfoToDynamix(Bundle bundle) {
        alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
        alert.putString(PluginConstants.ALERT_TOAST_VALUE, "Sending ip address to pairing server...");
        alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
        sendPluginAlert(alert);

        Result systemLevelOperationResult = dynamixSystemLevelOperation(bundle);
        if (!systemLevelOperationResult.wasSuccessful()) {
            Log.w(getClass().getSimpleName(), "Error updating pairing server");
            alert = new Bundle();
            alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
            alert.putString(PluginConstants.ALERT_TOAST_VALUE, "Error updating pairing server.");
            alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
            sendPluginAlert(alert);
        }
    }

    private int generateThreeDigitRandomNumber() {
        int max = 999;
        int min = 100;
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private String generatePairCode() {
        return generateThreeDigitRandomNumber() + " " + generateThreeDigitRandomNumber() + " " +
                generateThreeDigitRandomNumber() + " " + generateThreeDigitRandomNumber();
    }

}