package pluginviews;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.IPluginView;

public class PairingCodeView implements IPluginView {
    private ActivityController controller;
    Context context;
    String pairingCode;
    private final String PAIR_HINT = "Enter this pairing code on the Remote Client";

    public PairingCodeView(Context context, String pairingCode) {
        this.context = context;
        this.pairingCode = pairingCode;
    }

    @Override
    public void setActivityController(ActivityController controller) {
        this.controller = controller;
    }

    @Override
    public View getView() {
        LinearLayout lLayout = new LinearLayout(context);
        lLayout.setOrientation(LinearLayout.VERTICAL);
        lLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        lLayout.setGravity(Gravity.CENTER);
        TextView codeTextView = new TextView(context);
        codeTextView.setText(pairingCode);
        codeTextView.setTextSize(24);//24sp
        codeTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        lLayout.addView(codeTextView);
        TextView hintTextView = new TextView(context);
        hintTextView.setText(PAIR_HINT);
        hintTextView.setTextSize(18);//24sp
        hintTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        hintTextView.setGravity(Gravity.CENTER);
        lLayout.addView(hintTextView);
        return lLayout;
    }

    @Override
    public void destroyView() {
        controller.closeActivity();

    }

    @Override
    public void handleIntent(Intent intent) {

    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}
